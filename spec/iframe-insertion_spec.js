describe('Iframe Insertion', function(){

    beforeAll(function(done){
        require(['combined/iframe-insertion', 'https://code.jquery.com/jquery-3.3.1.min.js'], function(iframeInsertion, jq){
            window.AJS = window.AJS || {};
            window.AJS.$ = window.$;
            done();
        });
    });

    var IFRAME_INSERTION_FIXTURE_ID = 'iframe-insertion-fixture-div';

    beforeEach(function(){
        var containerFixtureDiv = document.createElement('div');
        containerFixtureDiv.id = IFRAME_INSERTION_FIXTURE_ID;
        document.body.appendChild(containerFixtureDiv);
        window.connectHost = {
            getExtensions: function(){},
            create: function(){
                var fakeExtensionNode = document.createElement('div');
                fakeExtensionNode.id = 'fake-extension-id';
                var $fakeExtension = AJS.$(fakeExtensionNode);
                $fakeExtension.addClass('fake-extension ap-iframe-container');
                return $fakeExtension;
            },
            destroy: function(){}
        };
    });

    afterEach(function(){
        var fixtureDiv = document.getElementById(IFRAME_INSERTION_FIXTURE_ID);
        fixtureDiv.parentNode.removeChild(fixtureDiv);
        delete window.connectHost;
    });

    

    it('appendConnectAddon removes existing iframes', function(){
        var numberOfAddons = 0;
        var data = {
            url: 'http://www.example.com',
            uniqueKey: 'a-unique-key',
            addon_key: 'addon-key',
            key: 'module-key',
            extension_id: 'fake-extension-id',
            hostFrameOffset: 0, // trick the insert code since karma runs in a frame
            options: {
                uniqueKey: 'a-unique-key'
            },
            extension: {
                options: {
                    uniqueKey: 'a-unique-key'
                }
            }
        };

        window.connectHost.getExtensions = function(){
            var extensions = [];
            for(var i =0; i<numberOfAddons; i++){
                extensions.push(data);
            }
            return extensions;
        }

        var fixtureDiv = document.getElementById(IFRAME_INSERTION_FIXTURE_ID);
        var containerId = 'embedded-' + data.uniqueKey;
        var containerDiv = document.createElement('div');
        containerDiv.id = containerId;
        fixtureDiv.appendChild(containerDiv);

        window._AP.appendConnectAddon(data);
        expect(document.querySelectorAll('.fake-extension').length).toEqual(1);
        numberOfAddons = 1;
        window._AP.appendConnectAddon(data);
        expect(document.querySelectorAll('.fake-extension').length).toEqual(1);
    });
});