/**
* this takes connect add-on options
* standardizes the weirdness of the connect plugin data
* gets iframes inserted into the DOM when circumstances are not ideal
* this is for all.js and the products.
**/
(function(){
  var aniFrame = (function(){
    return window.requestAnimationFrame  ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame ||
          function( callback ){
            window.setTimeout(callback, 50);
          };
  })();

  var getXDM = function(){
    return (window.connectHost || window.AP);
  };

  var getRenderedExtension = function(XDM, filter) {
    var existingExtensions = XDM.getExtensions(filter);
    return existingExtensions.filter(function(extension){
      return Boolean(document.getElementById(extension.extension_id));
    });
  };

  var loadModule = function(name, callback, retriesLeft) {
    try {
      var module = require(name);
      callback(module);
    } catch (error) {
      if (retriesLeft <= 0) {
        console.error('Unable to load module: ' + name);
        callback(null);
      } else {
        setTimeout(function () {
          loadModule(name, callback, retriesLeft - 1);
        }, 500);
      }
    }
  };

  function decodeContext(ctx) {
    var decodedContext;
    if(typeof ctx === 'string' && ctx.length > 1) {
      try {
        decodedContext = JSON.parse(ctx);
      } catch(e) {
        console.error('ACJS: failed to decode context', ctx);
      }
    }

    if(typeof decodedContext !== 'object') {
      decodedContext = {};
    }
    return decodedContext;
  }

  window._AP = window._AP || {};
  window._AP.addonAttemptCounter = window._AP.addonAttemptCounter || {};

  window._AP._convertConnectOptions = function(data) {
    var convertedData = {
      url: data.url,
      ns: data.uniqueKey,
      addon_key: data.addon_key,
      key: data.key,
      containerId: "embedded-" + data.uniqueKey,
      options: {
        history: {
          state: ""
        },
        uniqueKey: data.uniqueKey,
        origin: data.origin,
        hostOrigin: data.hostOrigin,
        moduleType: data.moduleType,
        moduleLocation: data.moduleLocation,
        isFullPage: (data.general === "1"),
        autoresize: true,
        user: {
          timeZone: data.timeZone,
          fullName: data.fullName,
          uid: data.uid,
          ukey: data.ukey
        },
        productContext: decodeContext(data.productCtx),
        structuredContext: decodeContext(data.structuredContext),
        contextJwt: data.contextJwt,
        contextPath: data.cp,
        width: data.w || data.width,
        height: data.h || data.height,
        sandbox: data.sandbox,
        targets: {
            env: {
                resize: 'both'
            }
        }
      }
    };

    if (typeof data.contentClassifier === 'string') {
      convertedData.options.contentClassifier = data.contentClassifier;
    }

    if (typeof data.hostFrameOffset === 'number') {
      // In terms of iframe content, it is always 1 layer deeper than the window outside the iframe.
      convertedData.options.hostFrameOffset = data.hostFrameOffset + 1;
    }

    if(!window._AP.isSubHost){
      convertedData.options.history.state = (window.location.hash ? window.location.hash.substr(2) : "");
    }

    return convertedData;
  };

  function setRealUrl(iframe, data) {
    var contentWindow = iframe.contentWindow;
    var contentDoc = iframe.contentDocument;
    var contents = {
      type: 'set_inner_iframe_url',
      iframeData: data
    };

    var codeToInject = '(function(){ var w = window; for (var i=0; i<' + data.options.hostFrameOffset + '; i++){w = w.parent; } ' +
    'w.postMessage(' + JSON.stringify(contents) + ', "*");' +
    '}());';
    contentDoc.open();
    contentDoc.write('<script>' + codeToInject + '</script>');
    contentDoc.close();
  }

window._AP._createSub = function(cleanData) {
  var connectAddonFrame = document.createElement("iframe");
  var attributes = getXDM().subCreate(cleanData);
  attributes.width = cleanData.options.width || "";
  attributes.height = cleanData.options.height || "";
  attributes.style = "border:0;";
  attributes['class'] = "ap-iframe";
  attributes['data-addon-key'] = cleanData.addon_key;
  attributes['data-key'] = cleanData.key;
  // Remove the src for now. This line can be removed later when the REST API is updated to exclude JWT urls
  delete attributes.src;
  Object.getOwnPropertyNames(attributes).forEach(function(key){
    connectAddonFrame.setAttribute(key, attributes[key]);
  });

  return connectAddonFrame;
};

function findHostFrame(hostFrameOffset, initialFrame) {
  if (typeof hostFrameOffset !== 'number') {
    return window.top;
  }

  // Find actual host frame if the option is available
  var host = initialFrame || window;
  for (var i = 0; i < hostFrameOffset; i++) {
    host = host.parent;
  }

  return host;
}

function ACJSisDefined(globalName){
  return (typeof window[globalName] !== "undefined");
}
function ACJSawaitGlobal(globalName, callback, interval) {
  interval = interval || 100;
  if(ACJSisDefined(globalName)){
    callback(window[globalName]);
  } else {
    var intervalFn = setInterval(function(){
      if(ACJSisDefined(globalName)) {
        clearInterval(intervalFn);
        callback(window[globalName]);
      }
    }, interval);
  }

}

function appendConnectAddon(data){
  window._AP.isSubHost = (findHostFrame(data.hostFrameOffset) !== window);
  var convertedData = window._AP._convertConnectOptions(data);
  window._AP.addonAttemptCounter[convertedData.containerId] = 0;
  var iframe;
  if(window._AP.isSubHost) {
    // propagate global options to sub iframes
    if(window.AP && window.AP._data && window.AP._data.options && window.AP._data.options.globalOptions) {
      convertedData.options.globalOptions = window.AP._data.options.globalOptions;
    }
    iframe = window._AP._createSub(convertedData);
    doAppend(iframe, convertedData);
  } else {
    ACJSawaitGlobal('connectHost', function(ACJSHost){
      var existingExtensions = getRenderedExtension(ACJSHost, {
        addon_key: convertedData.addon_key,
        key: convertedData.key
      });

      existingExtensions.forEach(function(existingAddon){
        if(existingAddon.extension.options.uniqueKey === convertedData.options.uniqueKey) {
          var existingFrame = document.getElementById(existingAddon.extension_id);
          ACJSHost.destroy(existingAddon.extension_id);
          if(existingFrame) {
            AJS.$(existingFrame).closest('.ap-iframe-container').remove();
          }
        }
      }, this);

      iframe = ACJSHost.create(convertedData);
      doAppend(iframe, convertedData);
    });
  }  
}


function doAppend(iframeToAppend, convertedData) {
  var container = document.getElementById(convertedData.containerId);
  window._AP.addonAttemptCounter[convertedData.containerId]++;
  if(!container){
    // retry up to 10 times.
    if(window._AP.addonAttemptCounter[convertedData.containerId] <= 10){
      aniFrame(doAppend);
    }
  } else {
    delete window._AP.addonAttemptCounter[convertedData.containerId];

    if(window._AP.isSubHost) {
      container.appendChild(iframeToAppend);
      setRealUrl(iframeToAppend, convertedData);
    } else {
      // JIRA agile executes scripts inside html elements and executes them twice if updating from one issue to another.
      var duplicateFrame = container.querySelector('.ap-iframe-container');
      if (duplicateFrame) {
        duplicateFrame.parentNode.removeChild(duplicateFrame);
        if (AJS.log) {
          AJS.log('AJS: duplicate iframe removed', convertedData, container);
        }
      }
      iframeToAppend.appendTo(container);
      iframeToAppend.data('addon-key', convertedData.addon_key);
      iframeToAppend.data('key', convertedData.key);
    }

  }
}


window._AP.appendConnectAddon = function (data) {
  var isRunningTests = false;
  try {
     if(window.top.karma) { // this can throw an origin exception so put it in a try/catch
      isRunningTests = true;
     }
  } catch (e) {} // outside of tests this should always throw an exception

  if((window !== window.top) && !isRunningTests) {
    var callback = function (e) {
      if (e.source === window.top && e.data && e.data.hostFrameOffset !== undefined) {
        window.removeEventListener('message', callback);

        data.hostFrameOffset = e.data.hostFrameOffset;
        appendConnectAddon(data);
      }
    };
    window.addEventListener('message', callback);

    // Send request to top window to find out the host
    window.top.postMessage({type: 'get_host_offset'}, '*');
  } else {
    var emceeKeyRegex = /com\.atlassian\.(jira|confluence)\.emcee($|\.local|\.staging|\.development\..*)/g;
    if (emceeKeyRegex.test(data.addon_key)) {
      loadModule('ac/marketplace', function (marketplace) {
        if (marketplace) {
          ACJSawaitGlobal('connectHost', function(ACJSHost){
            ACJSHost.defineModule('marketplace', marketplace);
          });
        }
        appendConnectAddon(data);
      }, 20);
    } else {
      appendConnectAddon(data);
    }
  }

};

}());
